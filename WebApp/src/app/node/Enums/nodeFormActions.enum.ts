export enum NodeFormActionsEnum {
  CREATE = 'create',
  UPDATE = 'update'
}
