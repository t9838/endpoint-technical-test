import { NodeFormActions } from '../Types';

export interface NodeDTO {
  id: number;
  name: string;
  childrenNodes: NodeDTO[];
  parentNode?: NodeDTO;
}

export interface CreateNodeDTO {
  name: string;
  parentNodeId: number;
}

export interface UpdateNodeDTO {
  parentNodeId: number;
}

export interface FlatNodeDTO {
  expandable: boolean;
  data: NodeDTO;
  level: number;
}


export interface NodeEditFormDTO {
  id: number;
  name: string;
  moveFromParent: NodeDTO | null;
  moveToParent: NodeDTO | null;
}

export interface NodeFormDataDTO{
  action: NodeFormActions;
  node: NodeDTO;
}
