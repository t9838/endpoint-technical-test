import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, ReplaySubject } from 'rxjs';
import {
  NodeDTO,
  CreateNodeDTO,
  UpdateNodeDTO
} from '../DTO';
import {
  APIArrayResponseDTO,
  APIResponseDTO,
  APIFeedbackResponseDTO,
  HttpHeadersEnum,
  ToastService,
  ToastEnum
} from '../../@core';

const { API_URL } = environment;

@Injectable({
  providedIn: 'root',
})
export class NodeService {
  constructor(
    private readonly httpClient: HttpClient,
    private readonly toastService: ToastService
    ) {
    this.getAllNodes();
  }

  private nodeList: ReplaySubject<NodeDTO[]> = new ReplaySubject<NodeDTO[]>();

  private getAllNodes(): Observable<APIArrayResponseDTO<NodeDTO[]>> {
    const APIResponse = new ReplaySubject<APIArrayResponseDTO<NodeDTO[]>>();

    this.httpClient
      .get<APIArrayResponseDTO<NodeDTO[]>>(`${API_URL}/node`)
      .subscribe({
        next: (res) => {
          this.setNodeList(res.data.content);
          APIResponse.next(res);
        },
        error: (err) => {
          APIResponse.error(err);
        },
      });

    return APIResponse.asObservable();
  }

  public getAllNodeAvaiableParents(
    nodeId: number
  ): Observable<APIArrayResponseDTO<NodeDTO[]>> {
    const APIResponse = new ReplaySubject<APIArrayResponseDTO<NodeDTO[]>>();

    this.httpClient
      .get<APIArrayResponseDTO<NodeDTO[]>>(`${API_URL}/node/parent/${nodeId}`, {
        headers: {
          [HttpHeadersEnum.HIDE_LOADDING]: 'true',
        },
      })
      .subscribe({
        next: (res) => {
          APIResponse.next(res);
        },
        error: (err) => {
          APIResponse.error(err);
        },
      });

    return APIResponse.asObservable();
  }

  public createNewNode(newNode: CreateNodeDTO): Observable<APIResponseDTO<NodeDTO>> {
    const APIResponse = new ReplaySubject<APIResponseDTO<NodeDTO>>();

    this.httpClient
      .post<APIResponseDTO<NodeDTO>>(`${API_URL}/node`, newNode)
      .subscribe({
        next: (res) => {
          this.getAllNodes();
          APIResponse.next(res);
          this.toastService.showToast({
            data: {
              content: `Node ${newNode.name} was successfully created`
            },
            type: ToastEnum.SUCCESS
          });
        },
        error: (err) => {
          APIResponse.error(err);
        },
      });

    return APIResponse.asObservable();
  }

  public editNode(nodeId: number, updatedNode: UpdateNodeDTO) {
    const APIResponse = new ReplaySubject<
      APIResponseDTO<APIFeedbackResponseDTO>
    >();

    this.httpClient
      .put<APIResponseDTO<APIFeedbackResponseDTO>>(
        `${API_URL}/node/${nodeId}`,
        updatedNode
      )
      .subscribe({
        next: (res) => {
          this.getAllNodes();
          APIResponse.next(res);
          this.toastService.showToast({
            data: {
              content: `Node ${nodeId} was successfully updated`
            },
            type: ToastEnum.SUCCESS
          });
        },
        error: (err) => {
          APIResponse.error(err);
        },
      });

    return APIResponse.asObservable();
  }

  public deleteNode(nodeId: number) {
    const APIResponse = new ReplaySubject<
      APIResponseDTO<APIFeedbackResponseDTO>
    >();

    this.httpClient
      .delete<APIResponseDTO<APIFeedbackResponseDTO>>(
        `${API_URL}/node/${nodeId}`
      )
      .subscribe({
        next: (res) => {
          this.getAllNodes();
          APIResponse.next(res);
          this.toastService.showToast({
            data: {
              content: `Node ${nodeId} was successfully deleted`
            },
            type: ToastEnum.SUCCESS
          });
        },
        error: (err) => {
          APIResponse.error(err);
        },
      });

    return APIResponse.asObservable();
  }

  public getNodeList(): Observable<NodeDTO[]> {
    return this.nodeList.asObservable();
  }

  private setNodeList(newNodeList: NodeDTO[]) {
    this.nodeList.next(newNodeList);
  }
}
