import { Routes } from '@angular/router';
import { NodeManagementComponent } from '../Components';
import { NodeRouteNamesEnum } from './node-route-names.enum';

export const CooperatedRoutes: Routes = [
  {
    path: NodeRouteNamesEnum.MANAGEMENT,
    component: NodeManagementComponent
  },
  {
    path: '**',
    redirectTo: NodeRouteNamesEnum.MANAGEMENT
  }
]
