import { NodeFormActionsEnum } from '../Enums';

export declare type NodeFormActions = NodeFormActionsEnum.CREATE | NodeFormActionsEnum.UPDATE
