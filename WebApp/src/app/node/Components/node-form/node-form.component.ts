import {
  Component,
  Inject,
  OnInit,
  OnDestroy,
  HostListener,
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@ngneat/reactive-forms';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { NodeService } from '../../Service';
import {
  NodeDTO,
  NodeFormDataDTO,
  CreateNodeDTO,
  UpdateNodeDTO,
  NodeEditFormDTO
} from '../../DTO';
import { NodeFormActionsEnum } from '../../Enums';

@Component({
  selector: 'app-node-form',
  templateUrl: './node-form.component.html',
  styleUrls: ['./node-form.component.scss'],
})
export class NodeFormComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];

  public action = NodeFormActionsEnum.CREATE;
  public createForm!: FormGroup<CreateNodeDTO>;
  public editForm!: FormGroup<NodeEditFormDTO>;
  public NodeFormActionsEnum = NodeFormActionsEnum;
  public avaiableParents:NodeDTO[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: NodeFormDataDTO,

    private readonly mdDialogRef: MatDialogRef<NodeFormComponent>,
    private readonly formBuilder: FormBuilder,
    private readonly nodeService: NodeService
  ) {}

  ngOnInit(): void {
    this.action = this.data.action;
    this.action === NodeFormActionsEnum.CREATE
      ? this.initCreateForm()
      : this.initUpdateForm();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => {
      if (!subscription.closed) {
        subscription.unsubscribe();
      }
    });
  }

  private initCreateForm() {
    this.createForm = this.formBuilder.group<CreateNodeDTO>({
      name: ['', [Validators.required]],
      parentNodeId: [this.data.node.id, [Validators.required]],
    });
  }

  private initUpdateForm() {
    this.searchAvaiableParents();

    this.editForm = this.formBuilder.group<NodeEditFormDTO>({
      id: [this.data.node.id, [Validators.required]],
      name: [
        {
          value: this.data.node.name,
          disabled: true,
        },
        [Validators.required],
      ],
      moveFromParent: [
        {
          value: this.data.node.parentNode ?? null,
          disabled: true
        },
        [Validators.required],
      ],
      moveToParent: [this.data.node.parentNode ?? null , [Validators.required]],
    });
  }

  private searchAvaiableParents() {
    const AvaiableParentsSubscription = this.nodeService
      .getAllNodeAvaiableParents(this.data.node.id)
      .subscribe((res) => {
        this.avaiableParents = res.data.content;
      });

      this.subscriptions.push(AvaiableParentsSubscription)
  }

  private close(value: boolean) {
    this.mdDialogRef.close(value);
  }

  @HostListener('keydown.esc')
  public onEsc() {
    this.close(false);
  }

  public cancel() {
    this.close(false);
  }

  public confirm() {
    this.action === NodeFormActionsEnum.CREATE
    ? this.createNode()
    : this.updateNode();
  }

  private createNode(){
    if(this.createForm.valid){
      const NewNode = this.createForm.value;
      const CreateNodeSubscription = this.nodeService.createNewNode(NewNode).subscribe(() => {
        this.close(true);
      })
      this.subscriptions.push(CreateNodeSubscription)
    }
  }

  private updateNode(){
    if(this.editForm.valid){

      const NodeFromForm = this.editForm.value;
      const NodeToUpdate: UpdateNodeDTO = {
        parentNodeId: NodeFromForm.moveToParent?.id ?? 0
      }

      const UpdateNodeSubscription = this.nodeService.editNode(NodeFromForm.id, NodeToUpdate).subscribe(() => {
        this.close(true);
      })
      this.subscriptions.push(UpdateNodeSubscription)
    }
  }
}
