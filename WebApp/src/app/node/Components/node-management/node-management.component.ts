import { Component, OnInit, OnDestroy } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { NodeService } from '../../Service';
import { NodeDTO, FlatNodeDTO } from '../../DTO';
import { NodeFormActions } from '../../Types';
import { NodeFormActionsEnum } from '../../Enums';
import { NodeFormComponent } from '../node-form/node-form.component';
import { ConfirmDialogService } from '../../../@core';

@Component({
  selector: 'app-node-management',
  templateUrl: './node-management.component.html',
  styleUrls: ['./node-management.component.scss']
})
export class NodeManagementComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  private treeFlattener!: MatTreeFlattener<NodeDTO, FlatNodeDTO>;
  public treeControl!: FlatTreeControl<FlatNodeDTO>;
  public dataSource!: MatTreeFlatDataSource<NodeDTO, FlatNodeDTO>;


  constructor(
    private readonly nodeService: NodeService,
    private readonly matDialog: MatDialog,
    private readonly confirmDialogService: ConfirmDialogService
  ) {
    this.setDatasource([]);
  }

  ngOnInit(): void {
    this.initSubscriptions();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      if(!subscription.closed){
        subscription.unsubscribe();
      }
    })
  }

  private initSubscriptions(): void{
    const NodeSubscription = this.nodeService.getNodeList().subscribe(res => {
      this.setDatasource(res)
    });

    this.subscriptions.push(NodeSubscription);
  }

  private setDatasource(data: NodeDTO[]){
   this. treeControl = new FlatTreeControl<FlatNodeDTO>(
      node => node.level,
      node => node.expandable,
    );

    this.treeFlattener = new MatTreeFlattener(
      this._transformer,
      node => node.level,
      node => node.expandable,
      node => node.childrenNodes,
    );

    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    this.dataSource.data = this._processData(data);
    this.treeControl.expandAll();
  }

  private _transformer = (node: NodeDTO, level: number): FlatNodeDTO => {
    const FlatNode: FlatNodeDTO = {
      expandable: !!node.childrenNodes && node.childrenNodes.length > 0,
      data: node,
      level: level
    }
    return FlatNode
  };

  private _processData = (data: NodeDTO[], parent?: NodeDTO): NodeDTO[] =>{

    data.forEach(node => {
      if (parent) {
        node.parentNode = parent;
      }

      if (node.childrenNodes) {
        this._processData(node.childrenNodes, node);
      }
    });

    return data;

  }

  public hasChild = (_: number, node: FlatNodeDTO): boolean => {
    return node.expandable
  }

  private OpenNodeForm(action: NodeFormActions, node:NodeDTO){
    this.matDialog.open(NodeFormComponent, {
      data: {
        action,
        node
      }
    });
  }

  public createNewNode(node: NodeDTO){
    this.OpenNodeForm(NodeFormActionsEnum.CREATE, node);
  }

  public editNode(node: NodeDTO){
    this.OpenNodeForm(NodeFormActionsEnum.UPDATE, node);
  }

  public deleteNode(node: NodeDTO){
    this.confirmDialogService.open({
      title: `Delete node`,
      message: `Are you sure you want to delete node <strong>${node.name}</strong>?`,
      cancelText: 'Cancel',
      confirmText: 'Confirm'
    })

    const ConfirmModalSubscription = this.confirmDialogService.confirmed().subscribe(res => {
      if(res){
        this.nodeService.deleteNode(node.id)
      }
    });

    this.subscriptions.push(ConfirmModalSubscription);
  }

}
