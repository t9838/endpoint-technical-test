import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { FlexLayoutModule } from '@angular/flex-layout';

import { NodeManagementComponent, NodeFormComponent } from '../Components';
import { NodeService } from '../Service';
import { CooperatedRoutes } from '../Routes';

import { CoreModule } from '../../@core';


const Components = [
  NodeManagementComponent,
  NodeFormComponent
]

const Modules = [
  RouterModule.forChild(CooperatedRoutes),
  HttpClientModule,
  CoreModule,
  FormsModule,
  ReactiveFormsModule,
  FlexLayoutModule
]

const Providers = [
  NodeService
]

@NgModule({
  declarations: [
    ...Components
  ],
  imports: [
    CommonModule,
    ...Modules
  ],
  providers: [
    ...Providers
  ]
})
export class NodeModule { }
