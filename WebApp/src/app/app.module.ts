import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CoreModule } from './@core';
import { SimplebarAngularModule } from 'simplebar-angular';

const Modules = [
  CoreModule,
  SimplebarAngularModule
]

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ...Modules
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
