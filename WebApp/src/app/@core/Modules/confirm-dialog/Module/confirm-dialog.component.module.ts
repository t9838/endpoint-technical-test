import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from '../Components';
import { ConfirmDialogService } from '../Service';
import { MaterialModule } from '../../material';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [ ConfirmDialogComponent ],
  exports: [ ConfirmDialogComponent ],
  entryComponents: [ ConfirmDialogComponent ],
  providers: [ ConfirmDialogService ],
})
export class ConfirmDialogModule {}
