export class ConfirmDialogOptionsDTO {
  title: string = 'titulo';
  message: string = 'mensagem';
  cancelText?: string;
  confirmText?: string;
}
