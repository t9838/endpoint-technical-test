import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { LoadingSpinnerComponent } from '../Components';
import { InterceptorService } from '../Service';
import { LoadingInterceptor, ErrorInterceptor } from '../Interceptors';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { ToastModule } from '../../toast';

const Components = [
  LoadingSpinnerComponent
]

const Material = [
  MatProgressSpinnerModule,
  MatDialogModule
]

const Modules = [
  HttpClientModule,
  ToastModule,
  ...Material
]

const Providers = [
  InterceptorService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: LoadingInterceptor,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true
  }
]

@NgModule({
  declarations: [
    ...Components
  ],
  imports: [
    CommonModule,
    ...Modules
  ],
  providers: [
    ...Providers
  ]
})
export class InterceptorsModule { }
