import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterceptorsModule } from '../../interceptors';
import { PlataformModule } from '../../plataform';
import { MaterialModule } from '../../material';
import { ToastModule } from '../../toast';
import { ConfirmDialogModule } from '../../confirm-dialog';

const Modules = [
  InterceptorsModule,
  PlataformModule,
  MaterialModule,
  ToastModule,
  ConfirmDialogModule
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...Modules
  ],
  exports: [
    ...Modules
  ]
})
export class CoreModule { }
