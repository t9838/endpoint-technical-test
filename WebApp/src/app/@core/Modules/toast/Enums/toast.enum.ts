export enum ToastEnum {
  SUCCESS = 'success',
  DANGER = 'danger',
  WARNING = 'warning'
}
