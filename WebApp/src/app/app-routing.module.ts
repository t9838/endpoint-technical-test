import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RouteNamesEnum } from './route-names.enum';
import { NodeModule } from './node';

const routes: Routes = [
  {
    path: RouteNamesEnum.NODE,
    loadChildren: () => NodeModule
  },
  {
    path: '**',
    redirectTo: RouteNamesEnum.NODE
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
