import * as dotenv from 'dotenv';
dotenv.config();

export default {
  type: 'sqlite',
  database: `${process.env.DB_NAME}.sqlite`,
  logging: JSON.parse(process.env.DB_LOG),
  entities: [
      "src/**/*entity.ts"
  ],
  migrations: ["migrations/*.ts"],
  cli: {
    migrationsDir: 'migrations'
  }
}