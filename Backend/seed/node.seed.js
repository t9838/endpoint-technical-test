class NodeSeed {

    _connectionRef;

    constructor(connection){
        this._connectionRef = connection;
    }

    execNodeSeeds = async () => {
        await this._createRootNode();
    }
    
    _createRootNode = async () => {
        
        await this._connectionRef.query(`
            INSERT OR IGNORE INTO node_entity (id, name)
            VALUES (1, 'root')
        `)
    }

}

module.exports = { NodeSeed }