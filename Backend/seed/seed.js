const createConnection = require('typeorm').createConnection;
const dotenv = require('dotenv');
const NodeSeed = require('./node.seed').NodeSeed;
 
dotenv.config();

createConnection({
    type: 'sqlite',
    database: `${process.env.DB_NAME}.sqlite`,
    synchronize: JSON.parse(process.env.DB_SYNC),
    logging: JSON.parse(process.env.DB_LOG)
}).then(async connection => {
    await new NodeSeed(connection).execNodeSeeds();
    process.exit(0);
}).catch(err => {
    console.log(err)
    process.exit(1);
})