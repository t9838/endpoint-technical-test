import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    UseGuards
} from '@nestjs/common';
import { NodeService } from '../Service';
import {
    NodeDTO,
    CreateNodeDTO,
    UpdateNodeDTO,
    NodeParamsDTO
} from '../DTO';
import { FeedbackResponse, ProtectRootGuard } from '../../utils';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Node')
@Controller('node')
export class NodeController {

    constructor(
        private readonly nodeService: NodeService
    ){}

    @Post()
    async createNewNode(@Body() nodeToCreate: CreateNodeDTO): Promise<NodeDTO>{
        try{
            const Response = await this.nodeService.createNewNode(nodeToCreate);
            return Response
        }
        catch(err){
            throw err
        }
    }

    @Get()
    async getAllNodes(): Promise<NodeDTO[]>{
        try{
            const Response = await this.nodeService.getAllNodes();
            return Response
        }
        catch(err){
            throw err
        }
    }

    @Get(':id')
    async getNodeById(@Param() nodeParams: NodeParamsDTO): Promise<NodeDTO>{
        try{
            const Response = await this.nodeService.getNodeById(nodeParams.id);
            return Response
        }
        catch(err){
            throw err
        }
    }

    @Get('parent/:id')
    async getAllNodeAvaiableParentsById(@Param() nodeParams: NodeParamsDTO): Promise<NodeDTO[]>{
        try{
            const Response = await this.nodeService.getAllNodeAvaiableParent(nodeParams.id);
            return Response
        }
        catch(err){
            throw err
        }
    }

    @UseGuards(ProtectRootGuard)
    @Put(':id')
    async updateNodeById(@Param() nodeParams: NodeParamsDTO, @Body() nodeToUpdate: UpdateNodeDTO): Promise<FeedbackResponse>{
        try{
            const Response = await this.nodeService.updateNodeById(nodeParams.id, nodeToUpdate);
            return Response
        }
        catch(err){
            throw err
        }
    }

    @UseGuards(ProtectRootGuard)
    @Delete(':id')
    async deleteNodeById(@Param() nodeParams: NodeParamsDTO): Promise<FeedbackResponse>{
        try{
            const Response = await this.nodeService.deleteNodeById(nodeParams.id);
            return Response
        }
        catch(err){
            throw err
        }
    }
}
