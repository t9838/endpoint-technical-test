import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TreeRepository } from 'typeorm';
import { NodeEntity } from '../Entity';
import {
    NodeDTO,
    CreateNodeDTO,
    UpdateNodeDTO
} from '../DTO';
import {
    FeedbackResponse,
    ActionsEnum,
    StatusEnum
} from '../../utils';

@Injectable()
export class NodeService {

    constructor(
        @InjectRepository(NodeEntity)
        private readonly nodeRepository: TreeRepository<NodeEntity>,
    ){}

    public async createNewNode(nodeToCreate: CreateNodeDTO): Promise<NodeDTO>{
        try{
            const ParentNode = await this.getNodeById(nodeToCreate.parentNodeId);
            delete nodeToCreate.parentNodeId;
            const NewNode = await this.nodeRepository.save({ ...nodeToCreate, parentNode: ParentNode});
            return NewNode
        }
        catch(err){
            throw err
        }
    }

    public async getAllNodes(): Promise<NodeDTO[]>{
        try{
            const NodesList = await this.nodeRepository.findTrees();
            return NodesList
        }
        catch(err){
            throw err
        }
    }

    public async getAllNodeAvaiableParent(nodeId: number): Promise<NodeDTO[]>{
        try{
            const NodesList = await this.getAllNodes();
            const AvaiableParents = this.filterNodeTree(NodesList, nodeId).sort((a,b) => a.id - b.id );
            return AvaiableParents
        }
        catch(err){
            throw err
        }
    }

    public async getNodeById(nodeId: number): Promise<NodeDTO>{
        try{
            const FoundedNode = await this.nodeRepository.findOne({
                where: {
                    id: nodeId
                }
            });

            if(!FoundedNode){
                throw new HttpException('Node not founded', HttpStatus.NOT_FOUND)
            }

            return FoundedNode
        }
        catch(err){
            throw err
        }
    }

    public async updateNodeById(nodeId: number, nodeTopUpdate: UpdateNodeDTO): Promise<FeedbackResponse>{
        try{
            const FoundedNode = await this.getNodeById(nodeId);
            const ParentNode = await this.getNodeById(nodeTopUpdate.parentNodeId);

            await this.nodeRepository.update(nodeId, { ...FoundedNode, parentNode: ParentNode});

            const UpdateResponse: FeedbackResponse = {
                action: ActionsEnum.UPDATE,
                status: StatusEnum.SUCCESS,
                message: `Node '${FoundedNode.name}'  was succefully updated`
            }

            return UpdateResponse
        }
        catch(err){

            const UpdateResponse: FeedbackResponse = {
                action: ActionsEnum.UPDATE,
                status: StatusEnum.ERROR,
                message: err.message
            }

            if(err instanceof HttpException){
                throw new HttpException(UpdateResponse, err.getStatus())
            }

            throw new HttpException(UpdateResponse, HttpStatus.BAD_REQUEST)
        }
    }

    public async deleteNodeById(nodeId: number){
        try{
            const FoundedNode = await this.getNodeById(nodeId);

            await this.nodeRepository.delete(nodeId);

            const DeleteResponse: FeedbackResponse = {
                action: ActionsEnum.DELETE,
                status: StatusEnum.SUCCESS,
                message: `Node '${FoundedNode.name}' was succefully deleted`
            }

            return DeleteResponse
 
        }
        catch(err){
            const DeleteResponse: FeedbackResponse = {
                action: ActionsEnum.DELETE,
                status: StatusEnum.ERROR,
                message: err.message
            }

            if(err instanceof HttpException){
                throw new HttpException(DeleteResponse, err.getStatus())
            }

            throw new HttpException(DeleteResponse, HttpStatus.BAD_REQUEST)        
        }
    }

    private filterNodeTree(nodeList: NodeDTO[], filterNodeId: number): NodeDTO[]{
        const FilteredNodes:NodeDTO[] = [];

        nodeList.forEach(node => {
            if(node.id !== Number(filterNodeId)){

                if(node.childrenNodes){
                    FilteredNodes.push(...this.filterNodeTree(node.childrenNodes, filterNodeId))
                }

                delete node.childrenNodes;
                FilteredNodes.push(node);
            }

        })
        return FilteredNodes
    }
}
