import { Type } from 'class-transformer';
import {
    IsNotEmpty,
    IsString,
    IsNumber,
    IsObject,
    ValidateNested,
    IsNumberString,
    IsOptional
} from 'class-validator';

export class NodeDTO{
    
    @IsNotEmpty()
    @IsNumber()
    id: number;

    @IsNotEmpty()
    @IsString()
    name: string;
  
    @IsNotEmpty()
    @IsObject()
    @ValidateNested()
    @Type(() => NodeDTO)
    parentNode : NodeDTO

    @IsOptional()
    @IsObject()
    @ValidateNested()
    @Type(() => NodeDTO)
    childrenNodes: NodeDTO[]
}

export class CreateNodeDTO{
    
    @IsNotEmpty()
    @IsString()
    name: string;
  
    @IsNotEmpty()
    @IsNumber()
    parentNodeId: number
}

export class UpdateNodeDTO{
    
    @IsNotEmpty()
    @IsNumber()
    parentNodeId: number
}

export class NodeParamsDTO{
    
    @IsNotEmpty()
    @IsNumberString()
    id ?: number;
}