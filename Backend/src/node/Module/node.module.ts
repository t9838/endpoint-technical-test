import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NodeEntity } from '../Entity';
import { NodeController } from '../Controller';
import { NodeService } from '../Service';

@Module({
  imports: [
    TypeOrmModule.forFeature([NodeEntity]),
  ],
  controllers: [ NodeController ],
  providers: [ NodeService ]
})
export class NodeModule {}
