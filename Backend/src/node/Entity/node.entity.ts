import { Tree } from 'typeorm';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    TreeChildren,
    TreeParent
} from 'typeorm';

@Entity()
@Tree("materialized-path")
export class NodeEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  name: string;

  @TreeChildren()
  childrenNodes: NodeEntity[]

  @TreeParent({ onDelete: 'CASCADE' })
  parentNode: NodeEntity

}