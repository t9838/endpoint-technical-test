import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpStatus, ValidationPipe } from '@nestjs/common';
import {
  SwaggerModule,
  DocumentBuilder,
  SwaggerCustomOptions
} from '@nestjs/swagger';
import { config } from 'dotenv'

config();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  
  app.useGlobalPipes(new ValidationPipe({
    errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
    transform: true,
    forbidUnknownValues: true,
    whitelist: true,
  }));

  app.setGlobalPrefix('api');
  app.enableCors();

  const config = new DocumentBuilder()
  .setTitle('Backend - Endpoint')
  .setDescription('Backend built using NestJS 8 Framework for Endpoint technical Test')
  .setVersion('1.0')
  .build();

  const document = SwaggerModule.createDocument(app, config);

  const customOptions: SwaggerCustomOptions = {
    swaggerOptions: {
      persistAuthorization: true,
    },
    customSiteTitle: 'Swagger - Backend',
    useGlobalPrefix: true
  };

  SwaggerModule.setup('swagger', app, document, customOptions);

  await app.listen(process.env.API_PORT);
}
bootstrap();
