import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { config } from 'dotenv'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { InterceptorModule } from './interceptor';
import { NodeModule } from './node'

config();

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: `${process.env.DB_NAME}.sqlite`,
      autoLoadEntities: true,
      synchronize: JSON.parse(process.env.DB_SYNC),
      logging: JSON.parse(process.env.DB_LOG)
    }),
    InterceptorModule,
    NodeModule
  ],
  controllers: [ AppController ],
  providers: [ AppService ],
})
export class AppModule {}
