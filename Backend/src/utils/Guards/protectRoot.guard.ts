import { Injectable, CanActivate, ExecutionContext, HttpStatus } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GenerateDefaultErrorResponse } from '../../utils';

@Injectable()
export class ProtectRootGuard implements CanActivate {

    constructor(private reflector: Reflector){}

    canActivate(context: ExecutionContext): boolean {

        const TargetId = context.switchToHttp().getRequest().params['id'];
        const Action = context.switchToHttp().getRequest().method === 'PUT' ? 'update' : 'delete'

        if(Number(TargetId) === 1){
            throw GenerateDefaultErrorResponse(HttpStatus.FORBIDDEN, { message:`The root node is protected against ${Action} attempts`})
        }

        return true
    }
}