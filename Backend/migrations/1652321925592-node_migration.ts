import {MigrationInterface, QueryRunner} from "typeorm";

export class customMigration1652321925592 implements MigrationInterface {
    name = 'nodeMigration1652321925592'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "node_entity" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL, "mpath" varchar DEFAULT (''), "parentNodeId" integer, CONSTRAINT "UQ_844de63dd11bae1a2fcc90414c7" UNIQUE ("name"))`);
        await queryRunner.query(`CREATE TABLE "temporary_node_entity" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL, "mpath" varchar DEFAULT (''), "parentNodeId" integer, CONSTRAINT "UQ_844de63dd11bae1a2fcc90414c7" UNIQUE ("name"), CONSTRAINT "FK_672ca10441462d98f4e0c32a1df" FOREIGN KEY ("parentNodeId") REFERENCES "node_entity" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_node_entity"("id", "name", "mpath", "parentNodeId") SELECT "id", "name", "mpath", "parentNodeId" FROM "node_entity"`);
        await queryRunner.query(`DROP TABLE "node_entity"`);
        await queryRunner.query(`ALTER TABLE "temporary_node_entity" RENAME TO "node_entity"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "node_entity" RENAME TO "temporary_node_entity"`);
        await queryRunner.query(`CREATE TABLE "node_entity" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL, "mpath" varchar DEFAULT (''), "parentNodeId" integer, CONSTRAINT "UQ_844de63dd11bae1a2fcc90414c7" UNIQUE ("name"))`);
        await queryRunner.query(`INSERT INTO "node_entity"("id", "name", "mpath", "parentNodeId") SELECT "id", "name", "mpath", "parentNodeId" FROM "temporary_node_entity"`);
        await queryRunner.query(`DROP TABLE "temporary_node_entity"`);
        await queryRunner.query(`DROP TABLE "node_entity"`);
    }

}
